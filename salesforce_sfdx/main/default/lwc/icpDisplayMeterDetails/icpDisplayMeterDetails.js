import { LightningElement,api,track } from 'lwc';
import { OmniscriptBaseMixin } from 'vlocity_cmt/omniscriptBaseMixin';
import { convertToArray } from 'c/baseComponent';
export default class IcpDisplayMeterDetails extends OmniscriptBaseMixin(LightningElement) {
		_omniJsonData;

		@api finalMeterList = [];
		SelectedICPMeterDetails = new Array();
		isOfferDisplayed = false;

		@api
		set omniJsonData(omniData) {
				if(omniData){
						this._omniJsonData = JSON.parse(JSON.stringify(omniData));
						this.makeMeterdetailsList(this._omniJsonData);
				}
		}

		get omniJsonData() {
				return this._omniJsonData;
		}

		sendValtoOS(event)
		{
				this.numberFieldValue = parseInt(event.target.value);
				let meterid = event.target.dataset.key;

				if(this.finalMeterList && this.finalMeterList.length >0)
				{
						this.SelectedICPMeterDetails = JSON.parse(JSON.stringify(this.finalMeterList));
				}

				let foundIndex = this.SelectedICPMeterDetails.findIndex(x => x.meter === meterid);
				if(this.SelectedICPMeterDetails && foundIndex>=0 && !this.numberFieldValue)
				{
						this.SelectedICPMeterDetails[foundIndex].UsageKwh = '';
				}
				if(this.SelectedICPMeterDetails && foundIndex>=0 && this.numberFieldValue)
				{
						this.SelectedICPMeterDetails[foundIndex].UsageKwh = this.numberFieldValue;
				}

				this.finalMeterList = this.SelectedICPMeterDetails;
				this.omniApplyCallResp({'SelectedICPMeterDetails':this.finalMeterList});
		}

		makeMeterdetailsList(datatest)
		{
				if(datatest && datatest.selectedICPDataView && datatest.selectedICPDataView.meter){
						this.finalMeterList = [];
						let updateMeterDetails = new Array();
						let counter = 0;
						let meterList = convertToArray(datatest.selectedICPDataView.meter);
						let meterSet = new Set();
						
						meterList.forEach((entry) => {
							if(datatest?.selectedICPDataView?.type === "ELECTRICITY")
							{
								let ElectricitydeviceMeterList ={};
								if(entry.device){

										let deviceList = convertToArray(entry.device);
										deviceList.forEach((entry2)=>{

												if(entry2.registryContentCode && entry2.periodOfAvailability && isNaN(entry2.registryContentCode))
												{
														let meter = entry2.registryContentCode+''+entry2.periodOfAvailability+'-'+entry.id;
														if(!meterSet.has(meter)){
																if(datatest.SelectedICPMeterDetails && datatest.SelectedICPMeterDetails.length>0)
																{
																		this.finalMeterList = datatest.SelectedICPMeterDetails;
																}
																else{
																		counter++;
																		
																		ElectricitydeviceMeterList.meter = meter;
																		ElectricitydeviceMeterList.counter = counter;
																		ElectricitydeviceMeterList.value = 0;
																		this.finalMeterList.push(ElectricitydeviceMeterList);
																		meterSet.add(meter);
																}
														}


												}

										});
								}
						 }	
						 if(datatest?.selectedICPDataView?.type === "GAS")
						 {
							let GasdeviceMeterList ={};
							counter++;
							let meter = entry.id;
							GasdeviceMeterList.counter = counter;
							GasdeviceMeterList.meter = meter;
							this.finalMeterList.push(GasdeviceMeterList);
						 }
						})
				}	
				/*
				if(datatest && datatest.selectedICPDataView && datatest.selectedICPDataView.meter)
				{

						let meterList = datatest.selectedICPDataView.meter;
						let isMeterArray = Array.isArray(meterList);
						let updateMeterDetails = new Array();
						let counter = 0;

						this.finalMeterList = [];

						if(isMeterArray){

								meterList.forEach((entry) => {

										let deviceMeterList ={};
										let reg = '';
										let mid = '';

										let isDeviceNotEmpty = (entry.device) ? true : false;

										if(isDeviceNotEmpty){

												let deviceList = entry.device;
												let isDeviceArray = Array.isArray(deviceList);

												if(isDeviceArray)
												{
														deviceList.forEach((entry2)=>{


																if(entry2.registryContentCode && entry2.periodOfAvailability && isNaN(entry2.registryContentCode) && entry2.registryContentCode!==reg && entry.id!==mid)
																{
																		if(datatest.SelectedICPMeterDetails)
																		{
																				this.finalMeterList = datatest.SelectedICPMeterDetails;
																		}
																		else{
																				counter++;
																				let meter = entry2.registryContentCode+''+entry2.periodOfAvailability+'-'+entry.id;
																				deviceMeterList.meter = meter;
																				deviceMeterList.counter = counter;
																				this.finalMeterList.push(deviceMeterList);
																				reg = entry2.registryContentCode;
																				mid = entry.id;

																		}

																}


														})

												}if(!isDeviceArray){


														if(deviceList.registryContentCode && deviceList.periodOfAvailability && isNaN(deviceList.registryContentCode))
														{
																if(datatest.SelectedICPMeterDetails)
																{
																		this.finalMeterList = datatest.SelectedICPMeterDetails;
																}
																else{
																		let meter2 = deviceList.registryContentCode+''+deviceList.periodOfAvailability+'-'+entry.id;
																		counter++;
																		deviceMeterList.meter = meter2;
																		deviceMeterList.counter = counter;
																		this.finalMeterList.push(deviceMeterList);

																}

														}
												}

										}
								})
						}	
						if(!isMeterArray){


								if(meterList.device)
								{
										let deviceList2 = meterList.device;
										let isDeviceAnArray = Array.isArray(deviceList2);
										let deviceMeterList2 ={};
										let reg2 = '';
										let mid2 = '';

										if(isDeviceAnArray)
										{
												deviceList2.forEach((entry2)=>{


														if(entry2.registryContentCode && entry2.periodOfAvailability && isNaN(entry2.registryContentCode)&& entry2.registryContentCode!=reg2 && meterList.id!=mid2)
														{
																if(datatest.SelectedICPMeterDetails)
																{
																		this.finalMeterList = datatest.SelectedICPMeterDetails;
																}
																else{
																		let meter4 = entry2.registryContentCode+''+entry2.periodOfAvailability+'-'+meterList.id;
																		counter++;
																		deviceMeterList2.meter = meter4;
																		deviceMeterList2.counter = counter;
																		this.finalMeterList.push(deviceMeterList2);
																		reg2 = entry2.registryContentCode;
																		mid2=meterList.id;
																}

														}
												})
										}if(!isDeviceAnArray){

												if(deviceList2.registryContentCode && deviceList2.periodOfAvailability && isNaN(deviceList2.registryContentCode))
												{
														if(datatest.SelectedICPMeterDetails)
														{
																this.finalMeterList = datatest.SelectedICPMeterDetails;
														}
														else{
																let meter5 = deviceList2.registryContentCode+''+deviceList2.periodOfAvailability+'-'+meterList.id;
																counter++;
																deviceMeterList2.meter = meter5;
																deviceMeterList2.counter = counter;
																this.finalMeterList.push(deviceMeterList2);

														}

												}
										}
								}
						}

				}*/
		}

		mergeMeterwithOffer(datatest)
		{
				if(datatest && datatest.SelectedICPMeterDetails && datatest.OffersList)
				{
						//newOfferDisplayedCheck = datatest.isOfferDisplayed;
						let isOfferArray = Array.isArray(datatest.OffersList);
						let offerArray = datatest.OffersList;
						let ICPnetwork ='';
						let ICPpriceCategoryCode ='';
						let diffDays = 0;
						if(datatest?.ChosePlans?.DisplayMeter?.BillFrom && datatest?.ChosePlans?.DisplayMeter?.BillTo){
								let date1 = new Date(datatest.ChosePlans.DisplayMeter.BillFrom);
								let date2 = new Date(datatest.ChosePlans.DisplayMeter.BillTo);
								diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10); 
						}
						//let dateDiff = datatest.ChosePlans.DisplayMeter.BillTo - datatest.ChosePlans.DisplayMeter.BillFrom;
						if(datatest.selectedICPDataView)
						{
								ICPnetwork = datatest.selectedICPDataView.network;
								ICPpriceCategoryCode = datatest.selectedICPDataView.priceCategoryCode;
						}



						if(isOfferArray)
						{
								offerArray.forEach((offer)=>
																	 {
										let viewItems = [];
										if(offer.priceBook)
										{


												let isTarrifArray = Array.isArray(offer.priceBook.tariffList);
												let tarrifList = offer.priceBook.tariffList;
												let icpMeterList = datatest.SelectedICPMeterDetails;
												let isMeterArray = Array.isArray(datatest.SelectedICPMeterDetails);

												if(isTarrifArray && isMeterArray)
												{

														icpMeterList.forEach((icpMeter)=>{
																tarrifList.forEach((tarrif)=>{
																		let regCCode = icpMeter.meter.substring(0, icpMeter.meter.indexOf("-"));
																		let tarrifObj = JSON.parse(JSON.stringify(tarrif));
																		console.log('regCCode::',regCCode);
																		if(tarrif.meterConfig && tarrif.meterConfig == regCCode)
																		{

																				let obj = {};
																				for (const [key, value] of Object.entries(icpMeter)) {
																						obj[key] = value;
																				}
																				for (const [key, value] of Object.entries(tarrifObj)) {
																						obj[key] = value;
																				}


																				obj.usageDays = diffDays;
																				obj.pricingCategoryCode = ICPpriceCategoryCode;
																				obj.network = ICPnetwork;

																				viewItems.push(obj);
																		}
																})
														})

														offer.viewBreakdownOfferDetails = viewItems;
														offer.discount = "12";//default
														this.isOfferDisplayed = true;

												}
										}
								})

								this.omniApplyCallResp({'OffersList':offerArray});

						}
				}
		}
}