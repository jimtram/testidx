import { LightningElement } from 'lwc';
import { OmniscriptBaseMixin } from 'vlocity_cmt/omniscriptBaseMixin';
export default class BaseComponent extends OmniscriptBaseMixin(LightningElement) {


}
export function convertToArray(obj) {
	if (obj instanceof Array) {
			return obj;
	} else {
			return [obj];
	}
}

export function getAddressFromResponse(registryAddress){
	if(registryAddress){
			let streetNumber = upperCaseFirst(registryAddress.streetNumber);
			let streetName = upperCaseFirst(registryAddress.streetName);
			let suburb = upperCaseFirst(registryAddress.suburb);
			let town = upperCaseFirst(registryAddress.town);
			let state = upperCaseFirst(registryAddress.state);
			let postCode = registryAddress.postCode;

			let fullAddress = (streetNumber ? streetNumber : "")+(streetName ? " "+streetName : "")+" "+(suburb ? " "+suburb : "" )+(town ? " "+town : "")+(state ? " "+state : "")+( postCode ? " "+postCode : "");
			return fullAddress;
	}

}
function upperCaseFirst(str){
	if(str){
		str = str.toString().toLowerCase();
		return str.charAt(0).toUpperCase() + str.substring(1);
	}
	return "";
}