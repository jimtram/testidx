import { LightningElement,api,track} from 'lwc';
import { OmniscriptBaseMixin } from 'vlocity_cmt/omniscriptBaseMixin';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getAddressFromResponse,convertToArray } from 'c/baseComponent';
export default class IcpSearchResult extends OmniscriptBaseMixin(LightningElement) {


		_omniJsonData;

		//for view icp details
		openModal=false;
		modalBody;
		modalHeader;

		//for add to site modal
		addToSiteModal=false;
		addToSitesOptions;

		sites={};

		electricityDetails = [];
		gasDetails = [];
		lpgDetails =[];

		@api electricityCheckboxOptions = [];
		@api gasCheckboxOptions = [];
		@api lpgCheckboxOptions = [];
	
		@api showAddToSiteButton = false;
		
		@api enableAddToSites = "false";
		
		electricityCheckboxEmptyFlag = true;
		gasCheckboxEmptyFlag = true;
		lpgCheckboxEmptyFlag = true;

		showLoading = false;
		istypeahead = false;

		duplicateICP="";
		toastMessage="";
		showToastFlag=false;

		searchResultTableFlag = false;
		oldPxid;

		get omniJsonData() {
				return this._omniJsonData;
		}

		

		@api
		set omniJsonData(omniData) {
				if(omniData) {
						this._omniJsonData = JSON.parse(JSON.stringify(omniData));
						this.electricityCheckboxEmptyFlag = true;
						this.gasCheckboxEmptyFlag = true;
						this.lpgCheckboxEmptyFlag = true;
						this.searchResultTableFlag = false;
						this.getCheckboxDataFromOmniScript();
				}
		}
		handlecloseModalEvent(e){
			this.openModal=false;
		}
		handleAddToSiteCancelModalEvent(e){
			this.addToSiteModal=false;
		}
		handleCloseToastEvent(e){
				this.showToastFlag=false;
		}

		onSearchHandle()
		{
				this.showLoading = true;
				try{
				if(this._omniJsonData.ICPSearch)
				{
						let inputMap = {};
						let ICPSearchObj={};
						let addressDetailsObj={};

						if(this._omniJsonData.ICPSearch.SearchType && this._omniJsonData.ICPSearch.SearchType =="Address")
						{

								inputMap = {
										number: this._omniJsonData.ICPSearch.AddressDetails.number, 
										street:this._omniJsonData.ICPSearch.AddressDetails.street,
										suburb:this._omniJsonData.ICPSearch.AddressDetails.suburb,
										city:this._omniJsonData.ICPSearch.AddressDetails.city,
										region : this._omniJsonData.ICPSearch.AddressDetails.region,
										unit_identifier : this._omniJsonData.ICPSearch.AddressDetails.unit_identifier, 
										xcoord : this._omniJsonData.ICPSearch.AddressDetails.xcoord,
										ycoord : this._omniJsonData.ICPSearch.AddressDetails.ycoord

								};
								addressDetailsObj.AddressDetails = inputMap;
								addressDetailsObj.SearchType = this._omniJsonData.ICPSearch.SearchType;
								ICPSearchObj.ICPSearch = addressDetailsObj;
						}
						if(this._omniJsonData.ICPSearch.SearchType && this._omniJsonData.ICPSearch.SearchType =="ICPNo")
						{
								
								if(this._omniJsonData.ICPSearch.SearchLPG && this._omniJsonData.ICPSearch.SearchLPG=="Yes")
								{
										let inputMap = {
												number: this._omniJsonData.ICPSearch.AddressDetails.number, 
												street:this._omniJsonData.ICPSearch.AddressDetails.street,
												suburb:this._omniJsonData.ICPSearch.AddressDetails.suburb,
												city:this._omniJsonData.ICPSearch.AddressDetails.city,
												region : this._omniJsonData.ICPSearch.AddressDetails.region,
												unit_identifier : this._omniJsonData.ICPSearch.AddressDetails.unit_identifier, 
												xcoord : this._omniJsonData.ICPSearch.AddressDetails.xcoord,
												ycoord : this._omniJsonData.ICPSearch.AddressDetails.ycoord

										};
										addressDetailsObj.AddressDetails = inputMap;
								}
								else
										{
											addressDetailsObj.ICPNo = this._omniJsonData.ICPSearch.ICPNo;
										}

								ICPSearchObj.ICPSearch = addressDetailsObj;
						}
						let params = {
								sClassName: 'vlocity_cmt.IntegrationProcedureService',
								sMethodName: 'EQT_SearchICPRegistry',
								input: ICPSearchObj,
								options: '{}'
						};

						this.omniRemoteCall(params).then((response) => {

								let searchICPRegistryResponseResultObj = {};
								let searchICPRegistryResponseObj = {};
								this.showLoading = false;
								console.log('response::',response);
								searchICPRegistryResponseResultObj = response.result.IPResult.searchICPRegistryResponseResult;
								searchICPRegistryResponseObj.ANZSIC = response.result.IPResult.ANZSIC;
								searchICPRegistryResponseObj.searchICPRegistryResponseResult = searchICPRegistryResponseResultObj;



								this._omniJsonData.searchICPRegistryResponse = "";
								this.omniApplyCallResp({"searchICPRegistryResponse": this._omniJsonData.searchICPRegistryResponse});

								this.omniApplyCallResp({"searchICPRegistryResponse": searchICPRegistryResponseObj});

						});
						
				}
				}catch(e)
						{
								this.showLoading = false;
						}
		}

		getCheckboxDataFromOmniScript() {
				this.electricityCheckboxOptions = [];
				this.gasCheckboxOptions = [];
				this.lpgCheckboxOptions = [];

				// create electricity Checkbox Options
				if(this._omniJsonData && this._omniJsonData.searchICPRegistryResponse && this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult){

						if(this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfElectricity && this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfElectricity.electricity){

								this.oldPxid = this._pxid;
								let electricityList = convertToArray(this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfElectricity.electricity);
								electricityList.forEach((entry) => {
										this.searchResultTableFlag = true;
										this.electricityCheckboxOptions.push({ label: getAddressFromResponse(entry.registryAddress), icpNo: entry.ICPIdentifier, value: false });
								})
						}
						

						// create gas Checkbox Options
						if(this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfGas && this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfGas.gas){
								let gasList = convertToArray(this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfGas.gas);
								gasList.forEach((entry) => {
									this.searchResultTableFlag = true;
										this.gasCheckboxOptions.push({ label: getAddressFromResponse(entry.registryAddress), icpNo: entry.ICPIdentifier, value: false });
								})
						}
						

						// create lpg Checkbox Options
						if(this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult && this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfLpg && this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfLpg.lpg){
								let lgpList = convertToArray(this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfLpg.lpg);
								lgpList.forEach((entry) => {
									this.searchResultTableFlag = true;
										this.lpgCheckboxOptions.push({ label: getAddressFromResponse(entry.registryAddress), value: entry.registryAddress.pxid });
								})
						}
						
						this.electricityCheckboxEmptyFlag = (this.electricityCheckboxOptions && this.electricityCheckboxOptions.length > 0) ?  false : true;
						this.gasCheckboxEmptyFlag = (this.gasCheckboxOptions && this.gasCheckboxOptions.length > 0) ?  false : true;
						this.lpgCheckboxEmptyFlag = (this.lpgCheckboxOptions && this.lpgCheckboxOptions.length > 0) ?  false : true;
				}
		}

		onElectricityPreviewClick (event) {

				this.showLoading=true;

				let temp=event.currentTarget.dataset.info;

				this.modalHeader = temp;//-----Set Header Value-------

				//-----Call IP to get details for ICP No. 1-------
				var className = 'vlocity_cmt.IntegrationProcedureService';
				var classMethod = 'EQT_ICPSearchByNo';
				var inputMap = {ICPNo: temp};
				var params = {
						input: inputMap,
						sClassName: className,
						sMethodName: classMethod,
						options: '{}'
				};
				try {
						this.omniRemoteCall(params).then((ipResponse) => {

								//Response from IP
								console.log('responseServer', ipResponse);

								let modalDataObj = {};

								modalDataObj.electricModalType = true;//---Set Modal Flag----
								let electricityObj = ipResponse.result.IPResult.ICPNoResponse.arrayOfElectricity.electricity;

								modalDataObj.icpno = electricityObj.ICPIdentifier;
								modalDataObj.status = electricityObj.status;
								modalDataObj.retailer = electricityObj.retailer;
								modalDataObj.priceCategoryCode = electricityObj.priceCategoryCode;
								modalDataObj.network = electricityObj.network;
								modalDataObj.propertyName = electricityObj.registryAddress.propertyName;
								modalDataObj.ANZSICCode = electricityObj.ANZSICCode;
								modalDataObj.ANZSICDescription = electricityObj.ANZSICDescription;
								if(electricityObj.meter){
									let meterObj = convertToArray(electricityObj.meter);
									if (meterObj.some(e => e.AMIFlag === true)) {
										modalDataObj.smartMeter = "Yes";
									}else{
										modalDataObj.smartMeter = "No";
									}
								}
								
								//------get address from resp -----------
								modalDataObj.address = getAddressFromResponse(ipResponse.result.IPResult.ICPNoResponse.arrayOfElectricity.electricity.registryAddress);
								//----------------------------------------

								//------Set Modal Properties------
								this.modalBody = modalDataObj;
								this.openModal=true;
								//--------------------------------
								this.showLoading=false;
						});
				} catch(err) {
						console.log('ERR: response', err);
				}
				//-------------------------------
		}

		onElectricityCheckBoxSelected(event){
			this.showLoading=true;
			let checkedFlag = event.target.checked;
			let temp=event.currentTarget.dataset.info;
			if(!checkedFlag){
					if(this.sites && this.sites.electricityDetails){
							this.sites.electricityDetails.forEach((e,index, object) => {
									if(e.ICPIdentifier === temp){
											object.splice(index, 1);
									}
							});
					}

					this.showLoading=false;
			}
			
			if(checkedFlag){
					const className = 'vlocity_cmt.IntegrationProcedureService';
					const classMethod = 'EQT_ICPSearchByNo';
					let inputMap = {ICPNo: temp};
					let params = {
							input: inputMap,
							sClassName: className,
							sMethodName: classMethod,
							options: '{}'
					};
					try {
							this.omniRemoteCall(params).then((ipResponse) => {

									this.sites.hideFlag = true;
									this.sites.hideIcon = "utility:expand_alt";

									if(!this.sites.electricityDetails){
											this.sites["electricityDetails"]=[];
									}
									
									//---Site Address for Address Search Type---
									if(this._omniJsonData.ICPSearch && this._omniJsonData.ICPSearch["TAAddress-Block"] && this._omniJsonData.ICPSearch["TAAddress-Block"].TAAddress){
											this.sites.address = this._omniJsonData.ICPSearch["TAAddress-Block"].TAAddress;
											this.sites.pxid = this._omniJsonData.ICPSearch["TAAddress-Block"].pxid;
									}
									//--------

									let electricityObj = ipResponse.result.IPResult.ICPNoResponse.arrayOfElectricity.electricity;
									//------get address from resp -----------
									let fullAddress = getAddressFromResponse(electricityObj.registryAddress);
									electricityObj.fullAddress=fullAddress;

									//---Site Address for ICP Search Type---
									if(!this.sites.address){
										this.sites.address = fullAddress;
										this.sites.pxid = 0;
									}
									//---------

									if(electricityObj.meter){
										let meterObj = convertToArray(electricityObj.meter);
										if (meterObj.some(e => e.AMIFlag === true)) {
											electricityObj.smartMeter = "Yes";
										}else{
											electricityObj.smartMeter = "No";
										}
									}
									this.sites.electricityDetails.push(electricityObj);
									this.showLoading=false;
									
							});

					} catch(err) {
							console.log('ERR: response', err);
							this.showLoading=false;
					}

			}
	}

	onGasPreviewClick (event) {

				this.showLoading=true;

				let temp=event.currentTarget.dataset.info;

				this.modalHeader = temp;//-----Set Header Value-------

				//-----Call IP to get details for ICP No. -------
				var className = 'vlocity_cmt.IntegrationProcedureService';
				var classMethod = 'EQT_ICPSearchByNo';
				var inputMap = {ICPNo: temp};
				var params = {
						input: inputMap,
						sClassName: className,
						sMethodName: classMethod,
						options: '{}'
				};
				try {
					//Response from IP
						this.omniRemoteCall(params).then((ipResponse) => {
								
							let modalDataObj = {};
							let gasObj =  ipResponse.result.IPResult.ICPNoResponse.arrayOfGas.gas;

								modalDataObj.gasModalType = true;//---Set Modal Flag----

								modalDataObj.icpno = gasObj.ICPIdentifier;
								modalDataObj.status = gasObj.status;
								modalDataObj.retailer = gasObj.retailer;
								modalDataObj.priceCategoryCode = gasObj.priceCategoryCode;
								modalDataObj.network = gasObj.network;
								modalDataObj.propertyName = gasObj.registryAddress.propertyName;
								modalDataObj.ANZSICCode = gasObj.ANZSICCode;
								modalDataObj.ANZSICDescription = gasObj.ANZSICDescription;
								modalDataObj.smartMeter = gasObj.meter.AMIFlag==true?"Yes":"No";
								modalDataObj.lineFactor = gasObj.meter.lineFactor;
								//------get address from resp -----------
								modalDataObj.address = getAddressFromResponse(gasObj.registryAddress);
								//----------------------------------------

								//------Set Modal Properties------
								this.modalBody = modalDataObj;
								this.openModal=true;
								//--------------------------------
								this.showLoading=false;
						});
				} catch(err) {
						console.log('ERR: response', err);
				}
				//-------------------------------
		}
		onGasCheckBoxSelected(event){
			this.showLoading=true;
			let checkedFlag = event.target.checked;
			let temp=event.currentTarget.dataset.info;
			if(!checkedFlag){
					if(this.sites && this.sites.gasDetails){
							this.sites.gasDetails.forEach((e,index, object) => {
									if(e.ICPIdentifier === temp){
											object.splice(index, 1);
									}
							});
					}
					this.showLoading=false;
			}
			if(checkedFlag){

					var className = 'vlocity_cmt.IntegrationProcedureService';
					var classMethod = 'EQT_ICPSearchByNo';
					var inputMap = {ICPNo: temp};
					var params = {
							input: inputMap,
							sClassName: className,
							sMethodName: classMethod,
							options: '{}'
					};
					try {
							this.omniRemoteCall(params).then((ipResponse) => {

									this.sites.hideFlag = true;
									this.sites.hideIcon = "utility:expand_alt";

									if(!this.sites.gasDetails){
											this.sites["gasDetails"]=[];
									}

									//---Site Address for Address Search Type---
									if(this._omniJsonData.ICPSearch && this._omniJsonData.ICPSearch["TAAddress-Block"] && this._omniJsonData.ICPSearch["TAAddress-Block"].TAAddress){
										this.sites.address = this._omniJsonData.ICPSearch["TAAddress-Block"].TAAddress;
										this.sites.pxid = this._omniJsonData.ICPSearch["TAAddress-Block"].pxid;
									}
									//--------
									

									let gasObj = ipResponse.result.IPResult.ICPNoResponse.arrayOfGas.gas;
									//------get address from resp -----------
									let fullAddress = getAddressFromResponse(gasObj.registryAddress);
									gasObj.fullAddress=fullAddress;
									//---Site Address for ICP Search Type---
									if(!this.sites.address){
										this.sites.address = fullAddress;
										this.sites.pxid = 0;
									}
									//---------
									gasObj.smartMeter = gasObj.meter.AMIFlag==true?"Yes":"No";
									this.sites.gasDetails.push(gasObj);
									this.showLoading=false;
							});
					} catch(err) {
							console.log('ERR: response', err);
					}
					console.log(JSON.stringify(this.sites));
			}
	}

	onLpgCheckBoxSelected(event){
		this.showLoading=true;
		let checkedFlag = event.target.checked;
		let lpginfo = event.currentTarget.dataset.info;
		let temp=event.currentTarget.dataset.info;
		if(!this.sites.lpgDetails){
				this.sites["lpgDetails"]=[];
		}

		if(!checkedFlag){
				if(this.sites && this.sites.lpgDetails){
						this.sites.lpgDetails.forEach((l,index, object) => {
								if(l.ICPIdentifier === temp){
										object.splice(index, 1);
								}
						});
				}
				this.showLoading=false;
		}else{
				try{
						if(this._omniJsonData.ICPSearch && this._omniJsonData.ICPSearch["TAAddress-Block"] && this._omniJsonData.ICPSearch["TAAddress-Block"].TAAddress){
								this.sites.address = this._omniJsonData.ICPSearch["TAAddress-Block"].TAAddress;
								this.sites.pxid = this._omniJsonData.ICPSearch["TAAddress-Block"].pxid;
						}
						this.sites.hideFlag = true;
						this.sites.hideIcon = "utility:expand_alt";
						let lpgObj = JSON.parse(JSON.stringify(this._omniJsonData.searchICPRegistryResponse.searchICPRegistryResponseResult.arrayOfLpg.lpg));
						let fullAddress = getAddressFromResponse(lpgObj.registryAddress);
						lpgObj.address = fullAddress;
						lpgObj.pxid = this.sites.pxid;
						this.sites.lpgDetails.push(lpgObj);
				}catch(err) {
						console.log('ERR: response', err);
				}
				this.showLoading=false;
		}
}
		
		
		handleAddToSitesClick(e){

				this.showToastFlag = false;

				// validate atleast one checkbox selected before adding to site
				if(!(this.sites?.electricityDetails?.length>0 || this.sites?.gasDetails?.length>0 || this.sites?.lpgDetails?.length>0)){
					return;
				}

				if(this._omniJsonData.selectedICPDataForFlex && this._omniJsonData.selectedICPDataForFlex.sites && this._omniJsonData.selectedICPDataForFlex.sites.length>0){
						this.addToSitesOptions = new Array();
						this.addToSitesOptions.push({label:"Create New Site",value:"New"});
						for(let siteIndex in this._omniJsonData.selectedICPDataForFlex.sites){
							if(this._omniJsonData.selectedICPDataForFlex.sites[siteIndex].address){
								this.addToSitesOptions.push({label:this._omniJsonData.selectedICPDataForFlex.sites[siteIndex].address,value:siteIndex});
							}
						}
						this.addToSiteModal = true;
				}else{
						let selectedICPDataForFlex = {};
						selectedICPDataForFlex.sites = new Array();

						if(this._omniJsonData.selectedICPDataForFlex && this._omniJsonData.selectedICPDataForFlex.sites){
								selectedICPDataForFlex.sites = this._omniJsonData.selectedICPDataForFlex.sites;
						}

						//check duel fuel flag
						let eleFlag = false;
						let gasFlag = false;
						let lpgFlag = false;
						if(this.sites.electricityDetails){
								Array.prototype.forEach.call(this.sites.electricityDetails,child => {
										eleFlag = true;
								});
						}
						if(this.sites.gasDetails){
								Array.prototype.forEach.call(this.sites.gasDetails,child => {
										gasFlag = true;
								});
						}


						if(this.sites.lpgDetails){
								Array.prototype.forEach.call(this.sites.lpgDetails,child => {
										lpgFlag = true;
								});
						}

						this.sites.isDualFuel = eleFlag && gasFlag || eleFlag && lpgFlag || eleFlag && gasFlag && lpgFlag ? true : false;

						selectedICPDataForFlex.sites.push(this.sites);

						this.omniApplyCallResp({"selectedICPDataForFlex": selectedICPDataForFlex});

						//Reset UI after adding to sites
						this.handleResetAll();
				}

		}
		handleAddToSiteCloseModalEvent(e){

				this.duplicateICP = "";
				this.toastMessage="ICP has already been added to site";

				this.addToSiteModal = false;
				let currentVal = e.detail;//index - New/0,1,2
				let selectedICPDataForFlex = {};
				selectedICPDataForFlex.sites = new Array();

				if(this._omniJsonData.selectedICPDataForFlex && this._omniJsonData.selectedICPDataForFlex.sites){
						selectedICPDataForFlex.sites = JSON.parse(JSON.stringify(this._omniJsonData.selectedICPDataForFlex.sites));
				}

				//let foundIndex = selectedICPDataForFlex.sites.findIndex(x => x.pxid == this.sites.pxid);//currentVal
				let foundIndex = currentVal || -1;
				//If new site
				if(currentVal === "New"){
					this.processAddToSites(selectedICPDataForFlex);
				}else{
						if(selectedICPDataForFlex.sites.length > 0){
								this.processAddToSites(selectedICPDataForFlex,foundIndex);
						}
				}


				if(this.duplicateICP){
						this.toastMessage = this.duplicateICP+" "+this.toastMessage;
						this.showToastFlag = true;
				}

				//Reset UI after adding to sites
				this.handleResetAll()
				this.omniApplyCallResp({"selectedICPDataForFlex": selectedICPDataForFlex});
		}
		processAddToSites(selectedICPDataForFlex,foundIndex){

				let processAddToSitesFlag = false;
				//1. Electricity
				if(this.sites.electricityDetails){
						let eleWithoutDuplicate = new Array();
						for(let icpObj of this.sites.electricityDetails){
								if(!this.checkDuplicateICPinAllSites(icpObj,"E")){
										eleWithoutDuplicate.push(icpObj);
								}else{
										this.duplicateICP = this.duplicateICP ? this.duplicateICP+" "+icpObj.ICPIdentifier : icpObj.ICPIdentifier;
								}
						}
						if(eleWithoutDuplicate.length>0){
								if(foundIndex==undefined){
										this.sites.electricityDetails = eleWithoutDuplicate;
										processAddToSitesFlag = true;
								}else{
										if(selectedICPDataForFlex.sites[foundIndex].electricityDetails){
												selectedICPDataForFlex.sites[foundIndex].electricityDetails.push(...eleWithoutDuplicate);
										}else{
												selectedICPDataForFlex.sites[foundIndex].electricityDetails = eleWithoutDuplicate;
										}
								}
						}
				}

				//2. Gas
				if(this.sites.gasDetails){
						let gasWithoutDuplicate = new Array();
						for(let icpObj of this.sites.gasDetails){

								if(!this.checkDuplicateICPinAllSites(icpObj,"G")){
										gasWithoutDuplicate.push(icpObj);
								}else{
										this.duplicateICP = this.duplicateICP ? this.duplicateICP+" "+icpObj.ICPIdentifier : icpObj.ICPIdentifier;
								}
						}
						if(gasWithoutDuplicate.length>0){
								if(foundIndex==undefined){
										this.sites.gasDetails = gasWithoutDuplicate;
										processAddToSitesFlag = true;
								}else{
										if(selectedICPDataForFlex.sites[foundIndex].gasDetails){
												selectedICPDataForFlex.sites[foundIndex].gasDetails.push(...gasWithoutDuplicate);
										}else{
												selectedICPDataForFlex.sites[foundIndex].gasDetails = gasWithoutDuplicate;
										}
								}
						}
				}
				//3. LPG
				if(this.sites.lpgDetails){
					let lpgWithoutDuplicate = new Array();
					for(let icpObj of this.sites.lpgDetails){

						if(!this.checkDuplicateICPinAllSites(icpObj,"L")){
							lpgWithoutDuplicate.push(icpObj);
						}else{
							this.duplicateICP = this.duplicateICP ? this.duplicateICP+" "+icpObj.address : icpObj.address;
						}
					}
					if(lpgWithoutDuplicate.length>0){
						if(foundIndex==undefined){
							this.sites.lpgDetails = lpgWithoutDuplicate;
							processAddToSitesFlag = true;
						}else{
							if(selectedICPDataForFlex.sites[foundIndex].lpgDetails){
									selectedICPDataForFlex.sites[foundIndex].lpgDetails.push(...lpgWithoutDuplicate);
							}else{
									selectedICPDataForFlex.sites[foundIndex].lpgDetails = lpgWithoutDuplicate;
							}
						}
					}
				}
				if(foundIndex == undefined && processAddToSitesFlag){
					selectedICPDataForFlex.sites.push(this.sites);
				}
				let eleFlag = false;
				let gasFlag = false;
				let lpgFlag = false;
				if(selectedICPDataForFlex.sites[foundIndex]){
						if(selectedICPDataForFlex.sites[foundIndex]?.electricityDetails){
								Array.prototype.forEach.call(selectedICPDataForFlex.sites[foundIndex].electricityDetails,child => {
										eleFlag = true;
								});
						}
						if(selectedICPDataForFlex.sites[foundIndex]?.gasDetails){
								Array.prototype.forEach.call(selectedICPDataForFlex.sites[foundIndex].gasDetails,child => {
										gasFlag = true;
								});
						}
						if(selectedICPDataForFlex.sites[foundIndex]?.lpgDetails){
								Array.prototype.forEach.call(selectedICPDataForFlex.sites[foundIndex].lpgDetails,child => {
										lpgFlag = true;
								});
						}
						selectedICPDataForFlex.sites[foundIndex].isDualFuel = eleFlag && gasFlag || eleFlag && lpgFlag || eleFlag && gasFlag && lpgFlag ? true : false;		
				}

		}
		checkDuplicateICPinAllSites(icpObj,icpTypeFlag){
				//--Get All Existing Sites--
				let selectedICPDataForFlex = {};
				selectedICPDataForFlex.sites = new Array();

				if(this._omniJsonData.selectedICPDataForFlex && this._omniJsonData.selectedICPDataForFlex.sites){
						selectedICPDataForFlex.sites = JSON.parse(JSON.stringify(this._omniJsonData.selectedICPDataForFlex.sites));
				}
				//-----
				let duplicateFlag = false;
				for(let siteObj of selectedICPDataForFlex.sites){

						if(icpTypeFlag === "E"){
								if(siteObj.electricityDetails){
										let foundIndex = siteObj.electricityDetails.findIndex(x => x.ICPIdentifier == icpObj.ICPIdentifier);
										console.log(foundIndex);
										if(foundIndex >= 0){
												duplicateFlag = true;
												break;
										}
								}
						}
						else if(icpTypeFlag === "G"){
								if(siteObj.gasDetails){
										let foundElectricityIndex = siteObj.gasDetails.findIndex(x => x.ICPIdentifier == icpObj.ICPIdentifier);
										if(foundElectricityIndex >= 0){
												duplicateFlag = true;
												break;
										}
								}
						}
						else if(icpTypeFlag === "L"){

							if(siteObj.lpgDetails){
								let foundLpgIndex = siteObj.lpgDetails.findIndex(x => x.pxid == icpObj.pxid);
										if(foundLpgIndex >= 0){
												duplicateFlag = true;
												break;
										}
							}
						}
				}
				return duplicateFlag;
		}
		handleResetAll(){
				this.sites={};
				this.electricityCheckboxOptions = [];
				this.gasCheckboxOptions = [];
				this.lpgCheckboxOptions = [];

				this.template.querySelectorAll('lightning-input').forEach(element => {
						if(element.type === 'checkbox' || element.type === 'checkbox-button'){
								element.checked = false;
						}else{
								element.value = null;
						}      
				});
		}

		validateSitesList()
		{
			this._omniJsonData.ICPSearch.SitesAdded = "false";
			this.omniApplyCallResp({'ICPSearch':this._omniJsonData.ICPSearch});
		}
}